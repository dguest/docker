default: all

all: mlbase atlasos analysisbase atlasmlbase

mlbase: mlbase_ubuntu mlbase_centos

mlbase_ubuntu:
	docker build -f ml-base/Dockerfile \
	--cache-from ml-base:ubuntu \
	--build-arg PYTHON_VERSION_TAG=3.6.8 \
	--build-arg LINK_PYTHON_TO_PYTHON3=1 \
	-t atlasml/ml-base:latest \
	-t atlasml/ml-base:ubuntu \
	-t atlasml/ml-base:bionic \
	--compress .

mlbase_centos:
	docker build -f ml-base/Dockerfile.centos7 \
	--cache-from atlasml/ml-base:centos \
	--build-arg PYTHON_VERSION_TAG=3.6.8 \
	--build-arg LINK_PYTHON_TO_PYTHON3=0 \
	-t atlasml/ml-base:centos \
	-t atlasml/ml-base:centos7 \
	--compress .

atlasos:
	docker build -f centos7-atlasos/Dockerfile \
	--cache-from centos7-atlasos:latest \
	-t atlasml/centos7-atlasos:latest \
	--compress .

analysisbase: atlasos
	docker build -f centos7-analysisbase/Dockerfile \
	--cache-from atlasml/centos7-analysisbase:latest \
	--build-arg RELEASE=21.2.60 \
	-t atlasml/centos7-analysisbase:latest \
	--compress .

atlasmlbase: analysisbase
	docker build -f atlasml-base/Dockerfile \
	--cache-from atlasml/atlasml-base:latest \
	--build-arg PYTHON_VERSION_TAG=3.6.8 \
	--build-arg LINK_PYTHON_TO_PYTHON3=0 \
	-t atlasml/atlasml-base:latest \
	--compress .
